# Quickstart

Introducing the Waffler and some usage examples.

## Creating a client

### Creating an interface

```php
<?php // FooClient.php

namespace App\Clients;

use Waffler\Definitions\Attributes\Get;
use Waffler\Definitions\Attributes\PathParam;

interface FooClient
{
    #[Get('foo')]
    public function getAll(): array;
    
    #[Get('foo/{id}')]
    public function getById(#[PathParam('id')] int $id): array;
}
```

In Waffler, a client is just a regular PHP interface.
We use the PHP 8 Attributes to give the information about how
the Waffler must make the request to the api.

In the example above, when we call the `FooClient::getById()` method,
Waffler will do a `GET` request to `/foo/{id}`, the `{id}` will
be replaced with the argument `$id`, Waffler knows how to replace
thanks to the `#[PathParam('id')]` attribute.

### Creating an instance

```php
use App\Clients\FooClient;
use Waffler\Rest\Client;

$fooClient = Client::implements(
    FooClient::class,
    ['base_uri' => 'https://api.fooapp.com/']
);
```

To make use of FooClient, we just ask to Waffler to generate an implementation
for that interface. The `Waffler\Rest\Client` class can really generate
a concrete implementation for the interface provided in the first argument, this
means you can safely assign to a typed property or pass to any typed parameter of
any function.

### The client

```php
use Waffler\Rest\Client;
```
The Client provides a few methods, but only one method is needed
to start using this library: `Client::implements()`.
The method accepts two arguments, the first is the **fully qualified class name** of your interface, 
and the second is an array of [GuzzleHttp Options](https://docs.guzzlephp.org/en/stable/quickstart.html)
(optional).

```php
use Waffler\Rest\Client;

$client = Client::implements(
    YourInterface::class,
    ['base_uri' => 'https://your.base-uri.com/api/']
);
```

### Sending a request

To send a request, you just need to call the method of your client
like any regular object, and the response is automatically casted 
to the type delcared in the interface.

```php
use App\Clients\FooClient;
use Waffler\Rest\Client;

$fooClient = Client::implements(
    FooClient::class,
    ['base_uri' => 'https://api.fooapp.com/']
);

$fooItem = $fooClient->getById(1);
```

### Return types

Waffler allows the client interface methods to have different return types:  

- array
- string
- int
- void
- object
- \ArrayObject
- \Psr\Http\Message\ResponseInterface
- mixed
- no type
- \Waffler\Definitions\Contracts\Castable (special type)
- AutoMapped Interface (special type)

*Work in progress*