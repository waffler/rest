<?php

namespace Tests\Clients;

/**
 * Class IterableForTesting.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Tests\Clients
 */
class TraversableForTesting implements \IteratorAggregate
{
    public function __construct(
        private array $data
    ) { }

    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }
}