<?php

namespace Tests\Feature;

use PHPUnit\Framework\TestCase;
use Tests\Clients\ClassForExtendTesting;
use Tests\Clients\ClientSetup;
use Tests\Clients\TraitForTesting;

/**
 * Class AttributesTesting.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Tests
 * @coversNothing
 */
class AttributesTest extends TestCase
{
    use ClientSetup;

    public function test_TraitForTesting_must_be_used_by_test_case_client()
    {
        $classUses = class_uses($this->client);

        self::assertTrue(in_array(TraitForTesting::class, $classUses));

        self::assertTrue($this->client->initialized);
    }

    public function test_ClassForExtendTesting_must_be_extended_by_test_case_client_generated_implementation()
    {
        self::assertTrue($this->client instanceof ClassForExtendTesting);
    }
}