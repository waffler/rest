<?php

declare(strict_types=1);

namespace Waffler\Rest\Lib;

use Closure;
use ReflectionClass;
use Waffler\Rest\Lib\Attributes\ExtendsClass;
use Waffler\Rest\Lib\Attributes\UseTraits;
use Waffler\Rest\Traits\ImplementationHelper;

/**
 * Class Implementer
 *
 * This class generates an implementation of any interface at runtime.
 *
 * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package  Waffler\Rest\Lib
 * @template T of object
 * @template P of object
 * @internal For internal use only.
 */
class Implementer
{
    /**
     * Factory function cache.
     *
     * @var array<class-string, \Closure>
     */
    protected static array $cache;

    /**
     * The factory function that generates the interface implementation.
     *
     * @var Closure(): T
     */
    protected Closure $factory;

    /**
     * Implementer constructor.
     *
     * @param \ReflectionClass<T> $interface The interface to be implemented.
     *
     * @throws \Exception
     */
    public function __construct(protected ReflectionClass $interface)
    {
        $this->factory = static::$cache[$this->interface->getName()] ??= $this->generateFactory();
    }

    /**
     * Compiles a factory function that generates an anonymous class that
     * implements the given interface at runtime.
     *
     * @throws \Exception
     * @psalm-return Closure(): T
     */
    protected function generateFactory(): Closure
    {
        $sprintf = sprintf(
            'return fn(mixed $parent) => new class ($parent) %s implements %s { %s %s };',
            $this->getClassNameToExtend(),
            '\\' . $this->interface->getName(),
            $this->getUsableTraits(),
            $this->generateMethods()
        );
        return eval($sprintf);
    }

    /**
     * Retrieves the list of traits to use in the implementation.
     *
     * @return string
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    protected function getUsableTraits(): string
    {
        $traits = ['\\' . ImplementationHelper::class];

        foreach ($this->interface->getAttributes(UseTraits::class) as $reflectionAttribute) {
            foreach ($reflectionAttribute->newInstance()->traits as $trait) {
                $traits[] = '\\' . $trait;
            }
        }

        return 'use ' . join(',', $traits) . ';';
    }

    /**
     * Generates the interface methods implementation.
     *
     * @throws \Exception
     */
    protected function generateMethods(): string
    {
        $methods = '';
        foreach ($this->interface->getMethods() as $method) {
            $methods .= new MethodGenerator($method);
        }
        return $methods;
    }

    /**
     * Retrieves the class name to add as parent class.
     *
     * @return string
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    protected function getClassNameToExtend(): string
    {
        if (!empty($reflectionAttributes = $this->interface->getAttributes(ExtendsClass::class))) {
            return 'extends \\' . $reflectionAttributes[0]->newInstance()->class;
        }

        return '';
    }

    /**
     * Factory function to generate a new instance of the given http client interface.
     *
     * @param P $parentClient The parent is the actual class that handles the methods
     *                        and properties calls.
     *
     * @return T
     */
    public function make(object $parentClient): mixed
    {
        return $this->factory->call($this, $parentClient);
    }
}
