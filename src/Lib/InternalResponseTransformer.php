<?php

namespace Waffler\Rest\Lib;

use ArrayObject;
use Closure;
use Exception;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use ReflectionClass;
use Traversable;
use TypeError;
use Waffler\Definitions\Castable;
use Waffler\Definitions\MethodInterface;
use Waffler\Definitions\ResponseTransformer as Contract;
use Waffler\Rest\Traits\InteractsWithAutoMappedTypes;
use Waffler\Rest\Traits\ResponseDecoder;

/**
 * Class ResponseTransformer.
 *
 * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package  Waffler\Rest\Lib
 * @internal Please, don't extend this class, the methods could change
 *           at any time with no previous warning.
 */
class InternalResponseTransformer implements Contract
{
    use ResponseDecoder;
    use InteractsWithAutoMappedTypes;

    /**
     * @var array<class-string<\Traversable>, \Closure> $iterableCallbacks
     */
    protected static array $iterableCallbacks = [];

    /**
     * Register a callback for transforming an array to an \Traversable class.
     *
     * @param class-string<T> $type
     * @param \Closure        $transformer The transformer receives the
     *                                     data array as first argument.
     *
     * @template T of \Traversable
     * @throws \TypeError If the given type is not subtype of \Traversable
     */
    public static function registerDataHandler(string $type, Closure $transformer): void
    {
        if (!is_a($type, Traversable::class, true)) {
            throw new TypeError("The type must be Traversable.");
        }

        self::$iterableCallbacks[$type] = $transformer;
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface  $response
     * @param \Waffler\Definitions\MethodInterface $method
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle(ResponseInterface $response, MethodInterface $method): mixed
    {
        try {
            return $this->castToAutoMapped($response, $method);
        } catch (TypeError) {
            // Ignores the type error.
        }

        $returnType = $method->getReturnType();

        if (is_a($returnType, Castable::class, true)) {
            return $returnType::cast($response);
        }

        return $this->castToCommonTypes($method, $response);
    }

    /**
     * Try to cast the response to the common data types.
     *
     * @param \Waffler\Definitions\MethodInterface $method
     * @param \Psr\Http\Message\ResponseInterface  $response
     *
     * @return null|string|int|float|bool|array|ArrayObject|StreamInterface|ResponseInterface
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    protected function castToCommonTypes(MethodInterface $method, ResponseInterface $response): null|string|int|float|bool|array|ArrayObject|StreamInterface|ResponseInterface
    {
        return match ($method->getReturnType()) {
            'array' => $this->decode($response, $method),
            'void', 'null' => null,
            'bool' => true,
            'string' => $response->getBody()->getContents(),
            'int', 'float', 'double' => $response->getStatusCode(),
            'object', ArrayObject::class => new ArrayObject(
                $this->decode($response, $method),
                ArrayObject::ARRAY_AS_PROPS
            ),
            StreamInterface::class => $response->getBody(),
            ResponseInterface::class, Response::class, MessageInterface::class, 'mixed' => $response,
            default => throw new TypeError()
        };
    }

    /**
     * If the method returns an "auto mapped" object or list, it should be cast here.
     *
     * @param \Psr\Http\Message\ResponseInterface          $response
     * @param \Waffler\Definitions\MethodInterface<object> $method
     *
     * @return mixed
     * @throws \Exception
     */
    protected function castToAutoMapped(ResponseInterface $response, MethodInterface $method): mixed
    {
        if ($mappedInterface = $method->returnsAutoMapped()) {
            return $this->createProxy(
                $mappedInterface,
                $this->decode($response, $method)
            );
        }

        if ($mappedInterface = $method->returnsAutoMappedList()) {
            $getData = fn (): array => $this->createProxies(
                $mappedInterface,
                $this->decode($response, $method)
            );

            $returnType = $method->getReturnType();

            if ($returnType === 'array') {
                return $getData();
            }

            if ($callback = self::$iterableCallbacks[$returnType] ?? false) {
                return $callback($getData(), $response, $method);
            }

            try {
                // If we don't have a registered callback to transform the return type,
                // we'll assume the Traversable implementation receives the array as first
                // argument in the constructor. If we can't build the Traversable object,
                // we'll throw the type error.

                if (class_exists($returnType) && !$this->isInterface($returnType)) {
                    return new $returnType($getData());
                }
            } catch (Exception) {
            }
        }

        throw new TypeError();
    }

    /**
     * Checks if the given class-string is an interface.
     *
     * @param class-string $returnType
     *
     * @return bool
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    protected function isInterface(string $returnType): bool
    {
        try {
            return (new ReflectionClass($returnType))->isInterface();
        } catch (\ReflectionException) {
            return false;
        }
    }
}
