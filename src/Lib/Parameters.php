<?php

declare(strict_types=1);

namespace Waffler\Rest\Lib;

use Exception;
use InvalidArgumentException;
use ReflectionParameter;
use Waffler\Definitions\Attributes\Auth\Basic;
use Waffler\Definitions\Attributes\Auth\Bearer;
use Waffler\Definitions\Attributes\Auth\Digest;
use Waffler\Definitions\Attributes\Auth\Ntml;
use Waffler\Definitions\Attributes\Body;
use Waffler\Definitions\Attributes\FormParamItem;
use Waffler\Definitions\Attributes\FormParams;
use Waffler\Definitions\Attributes\HeaderParam;
use Waffler\Definitions\Attributes\Headers;
use Waffler\Definitions\Attributes\Json;
use Waffler\Definitions\Attributes\JsonParam;
use Waffler\Definitions\Attributes\Multipart;
use Waffler\Definitions\Attributes\PathParam;
use Waffler\Definitions\Attributes\Query;
use Waffler\Definitions\Attributes\QueryParam;
use Waffler\Definitions\Attributes\RawOptions;
use Waffler\Rest\Traits\InteractsWithAttributes;
use WeakMap;

/**
 * Class Parameters
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Rest\Lib
 * @internal
 */
class Parameters
{
    use InteractsWithAttributes;

    /**
     * @var \WeakMap<\ReflectionParameter,mixed>
     */
    protected WeakMap $parameterMap;

    /**
     * @param array<\ReflectionParameter> $reflectionParameters
     * @param array                       $arguments
     */
    public function __construct(protected array $reflectionParameters, protected array $arguments)
    {
        $this->loadParameterMap();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getQueryParams(): array
    {
        return $this->valuesForPair(
            Query::class,
            QueryParam::class
        );
    }

    /**
     * @return array<string,string>
     * @throws \Exception
     */
    public function getHeaderParams(): array
    {
        return $this->valuesForPair(
            Headers::class,
            HeaderParam::class
        ) + $this->getBearerParam();
    }

    /**
     * @return array<string,string>
     * @throws \Exception
     */
    public function getBearerParam(): array
    {
        $token = $this->valueFor(Bearer::class);
        if (is_null($token)) {
            return [];
        }
        return ['Authorization' => "Bearer $token"];
    }

    /**
     * @return array|null
     * @throws \Exception
     */
    public function getFormParams(): ?array
    {
        return $this->valuesForPair(
            FormParams::class,
            FormParamItem::class,
        );
    }

    /**
     * @return array|null
     * @throws \Exception
     */
    public function getMultipartParams(): ?array
    {
        return $this->valueFor(Multipart::class);
    }

    /**
     * @return array<string>|null
     * @throws \Exception
     * @psalm-suppress PropertyTypeCoercion
     */
    public function getAuthParams(): ?array
    {
        if ($value = $this->valueFor(Basic::class)) {
            $value[] = 'basic';
        } elseif ($value = $this->valueFor(Digest::class)) {
            $value[] = 'digest';
        } elseif ($value = $this->valueFor(Ntml::class)) {
            $value[] = 'ntml';
        }
        return $value;
    }

    /**
     * @throws \Exception
     */
    public function getBodyParam(): ?string
    {
        return $this->valueFor(Body::class);
    }

    /**
     * @return array<string,mixed>|null
     * @throws \Exception
     */
    public function getJsonParams(): ?array
    {
        return $this->valuesForPair(
            Json::class,
            JsonParam::class,
        );
    }

    /**
     * @return array<string,mixed>
     * @throws \Exception
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function getRawOptions(): array
    {
        return $this->valueFor(RawOptions::class) ?? [];
    }

    /**
     * @param string $path
     *
     * @return string
     * @throws \InvalidArgumentException If the Value of PathParam does not pass the type check.
     * @throws \Exception If the path param is not used, or is repeated, or has no replacement that use it.
     */
    public function parsePath(string $path): string
    {
        $pathParameters = $this->withAttributes(PathParam::class);
        foreach ($pathParameters as $pathParameter) {
            $attributeInstance = $this->getAttributeInstance($pathParameter, PathParam::class);
            $value = $this->get($pathParameter);
            AttributeChecker::check(PathParam::class, $value);
            $placeholder = $attributeInstance->name;
            $count = 0;
            $path = str_replace('{' . $placeholder . '}', (string)$value, $path, $count);
            if ($count === 0) {
                throw new Exception("The argument \"{$pathParameter->getName()}\" is not used by any path parameter.");
            } elseif ($count > 1) {
                throw new Exception("The path parameter \"$placeholder\" is repeated.");
            }
        }
        $missing = [];
        if (preg_match('/{.*?}/', $path, $missing)) {
            throw new Exception("The path parameter \"$missing[0]\" has no replacement");
        }
        return $path;
    }

    // protected

    /**
     * @param class-string<T> $attribute
     * @param mixed|null      $default
     *
     * @return mixed|R
     * @template T
     * @template R
     * @throws \Exception
     */
    protected function valueFor(string $attribute, mixed $default = null): mixed
    {
        $data = $this->valuesFor($attribute);
        if (count($data) > 1) {
            throw new Exception("Only one attribute of type {$attribute} are allowed");
        }
        return $data[0] ?? $default;
    }

    /**
     * @param class-string<T> $attribute
     *
     * @return array<int,mixed>
     * @template T
     */
    protected function valuesFor(string $attribute): array
    {
        $data = [];
        foreach ($this->reflectionParameters as $reflectionParameter) {
            if (!$this->doesItHasAttribute($reflectionParameter, $attribute)) {
                continue;
            }
            $value = $this->get($reflectionParameter);
            AttributeChecker::check($attribute, $value);
            $data[] = $value;
        }
        return $data;
    }

    /**
     * @param class-string<T> $listTypeAttribute
     * @param class-string<T> $singleTypeAttribute
     *
     * @return array
     * @throws \Exception
     * @template T
     */
    protected function valuesForPair(string $listTypeAttribute, string $singleTypeAttribute): array
    {
        $group = $this->valueFor($listTypeAttribute, []);
        foreach ($this->withAttributes($singleTypeAttribute) as $parameter) {
            $key = $parameter->getAttributes($singleTypeAttribute)[0]->getArguments()[0];
            $group[$key] = $this->get($parameter);
        }
        return $group;
    }

    /**
     * @param class-string<T> $attribute
     *
     * @return array<ReflectionParameter>
     * @template T
     */
    protected function withAttributes(string $attribute): array
    {
        return array_values(array_filter(
            $this->reflectionParameters,
            fn (ReflectionParameter $parameter) => $this->doesItHasAttribute($parameter, $attribute)
        ));
    }

    protected function loadParameterMap(): void
    {
        $this->parameterMap = new WeakMap();
        foreach ($this->reflectionParameters as $parameter) {
            $this->parameterMap[$parameter] =
                // Load by name or by position
                $this->arguments[$parameter->getName()] ??
                $this->arguments[$parameter->getPosition()] ??
                // If the parameter is not available by name or by position
                // we will try to get the default value. If the default value is not available,
                // we will throw an exception.
                (
                    $parameter->isDefaultValueAvailable()
                    ? $parameter->getDefaultValue() :
                    throw new InvalidArgumentException("Required argument {$parameter->name} is missing.")
                );
        }
    }

    protected function get(ReflectionParameter $parameter): mixed
    {
        return $this->parameterMap[$parameter];
    }
}
