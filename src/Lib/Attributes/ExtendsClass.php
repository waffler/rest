<?php

namespace Waffler\Rest\Lib\Attributes;

/**
 * Attribute ExtendsClass.
 *
 * You must use this attribute in your interface if you want to generate an implementation that inherits some class.
 *
 * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package  Waffler\Rest\Lib\Attributes
 * @template T of object
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class ExtendsClass
{
    /**
     * @param class-string<T> $class
     */
    public function __construct(
        public string $class
    ) {
    }
}
