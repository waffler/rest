<?php

namespace Waffler\Rest\Traits;

use Psr\Http\Message\ResponseInterface;
use Waffler\Definitions\MethodInterface;
use function Waffler\Rest\array_get;

/**
 * Trait ResponseDecoder.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package Waffler\Rest\Traits
 */
trait ResponseDecoder
{
    public function decode(ResponseInterface $response, MethodInterface $method): array
    {
        $response = (array)json_decode($response->getBody()->getContents(), true);
        if ($method->mustUnwrap() && !empty($response)) {
            return array_get($response, $method->getWrapperProperty());
        }
        return $response;
    }
}
